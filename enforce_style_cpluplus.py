#!/usr/bin/env python
#note: tested in python version 3.6.2
import fnmatch
import os
import subprocess
import argparse
import sys

if __name__=="__main__":
    print('argv {}'.format(sys.argv[1]))

    files = []

    for root, dirnames, filenames in os.walk(sys.argv[1]):
        #for filename in fnmatch.filter( filenames, '*.cpp'):
        for filename in fnmatch:
            if filename.endswith('*.cpp', '*.h', '*.c'):
                comb_filename = os.path.join(root, filename)
                files.append(os.path.join(root,filename))

    prog = "astyle"

    for f in files :
        print('#### Styling: {}'.format(f))
        subprocess.call([prog, "-q", "--options=astyle.conf", f])

    print("#### Finished styling...")
