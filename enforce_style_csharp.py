#!/usr/bin/env python
#note: tested in python version 3.7.2 
import fnmatch
import os
import subprocess
import argparse
import sys
        #for filename in fnmatch:

if __name__=="__main__":
    print('argv {}'.format(sys.argv[1]))

    files = []

    for root, dirnames, filenames in os.walk(sys.argv[1]):
        for filename in fnmatch.filter( filenames, '*.cs'):
            if filename.endswith('.cs'): #'*.cs' applicable in 3.6.2 
                comb_filename = os.path.join(root, filename)
                files.append(comb_filename)

    prog = "astyle"

    for f in files :
        print('#### Styling: {}'.format(f))
        subprocess.call([prog, "-q", "--options=astyle.conf", f])

    #remove the backups
    for root, dirnames, filenames in os.walk(sys.argv[1]):
        for filename in fnmatch.filter( filenames, '*.orig'):
            if filename.endswith('.orig'):
                comb_filename = os.path.join(root, filename)
                os.remove(comb_filename)
                print('#### Removed: {}'.format(filename))


    print("#### Finished styling...")
